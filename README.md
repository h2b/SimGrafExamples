# SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

These are examples demonstrating the use of SimGraf, the scala library of graphics utilities.

## Licence

SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

Copyright 2016-2017 Hans-Hermann Bode

Licensed under the EUPL V.1.1.
