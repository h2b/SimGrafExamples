organization := "de.h2b.scala.lib"

organizationName := "private"

organizationHomepage := Some(url("http://h2b.de"))

homepage := Some(url("http://h2b.de"))

startYear := Some(2016)

description := """
These are examples demonstrating the use of SimGraf, the scala library of graphics utilities.
"""

licenses := Seq("European Union Public Licence, v. 1.1" -> url("https://joinup.ec.europa.eu/community/eupl/og_page/eupl"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/SimGrafExamples.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
