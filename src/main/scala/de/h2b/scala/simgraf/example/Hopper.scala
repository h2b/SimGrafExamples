/*
  SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package de.h2b.scala.simgraf.example

import scala.math.{ abs, sqrt }

import de.h2b.scala.lib.phys.Time
import de.h2b.scala.lib.simgraf.{ Color, Point, World }
import de.h2b.scala.lib.util.{ Level, Logger, Timer }

/**
 * Draws repetitive patterns based on point iteration.
 *
 * Generates point coordinates following B. Martin  by consecutive iteration
 * depending on parameters {@code a, b, c} and draws them on the screen within a
 * world-coordinate system defined by {@code [p0.x, p0.x+l] x [p0.y, p0.y+l]}.
 *
 * Based on my Modula-2 module Huepfer of 1986-88.
 *
 * Copyright (C) 1986-1988, 2015-2017 Hans-Hermann Bode
 *
 * @see [1] A. K. Dewdney, Spektr. d. Wiss. 1986/11, 6-13 (1986)
 * @version 3.1
 * @author h2b
 */
class Hopper private (val a: Double, val b: Double, val c: Double)
    (val n: Int, val colint: Int)
    (val world: World, val colors: Stream[Color]) {

  private val log = Logger(getClass) at Level.Warn

	private val logint = 1000 //logging interval (log each logint step)

	run()

  private def run () = {
	  val t = Timer()
	  martin()
	  println("Time: " + Time(t.stop()))
  }

  private def martin () = {
    val colIter = colors.iterator
    var col = colIter.next() //active color
    var j, k = 0 //main and color interval count
    var x, y = 0.0
    while (j<=n) {
    	world.plot(Point(x, y), col)
    	val dy = sqrt(abs(b * x - c))
      val xx = if (x>=0) y-dy else y+dy
      y = a - x
      x = xx
      if (j==Int.MaxValue) j = 0 //in case n=0
      j += 1; k += 1
      if (k==colint) {
    	  col = colIter.next()
        k = 0
      }
      if (j % logint == 0) log.debug(s"count: $j")
    }
    log.debug(s"count: $j")
  }

}

object Hopper extends App {

	private val conf = config("hopper", args)

  try {
    val a = conf("a").toDouble
    val b = conf("b").toDouble
    val c = conf("c").toDouble
    val p0x = conf("p0.x").toDouble
    val p0y = conf("p0.y").toDouble
    val l = conf("l").toDouble
    val n = conf("n").toInt
    val colint = conf("colint").toInt
    val sl = conf("sl").toInt
    val bg = color(conf("bg"))
    val nfg = conf("nfg").toInt
    val title = conf("title")
    apply(a, b, c)(n, colint)(world(Point(p0x, p0y), l)
        (sl, title, Some(bg)), colorStream(nfg, Some(bg)))
  } catch {
    case t: Throwable ⇒ println(t.getMessage)
  }

	/**
	 * Draws repetitive patterns based on point iteration.
	 *
	 * @param a iteration parameter
	 * @param b iteration parameter
	 * @param c iteration parameter
	 * @param n number of points to generate (0 means ∞)
	 * @param colint color interval (color changes after {@code colint} iterations)
	 * @param world the coordinate system
   * @param colors stream of foreground colors to be used
	 * @return
	 */
	def apply (a: Double, b: Double, c: Double) (n: Int, colint: Int)
      (world: World, colors: Stream[Color]) =
    new Hopper(a, b, c)(n, colint)(world, colors)

}
