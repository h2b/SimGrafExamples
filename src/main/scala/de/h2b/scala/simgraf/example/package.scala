/*
  SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package de.h2b.scala.simgraf

import scala.util.{ Failure, Success, Try }

import de.h2b.scala.lib.io.getResource
import de.h2b.scala.lib.simgraf.{ Color, ColorIterator, Point, Screen, World }
import de.h2b.scala.lib.simgraf.layout.GridLayout
import de.h2b.scala.lib.simgraf.shapes.Rectangle
import de.h2b.scala.lib.util.cli.{ CommandLine, HelpParameter, MainParameter }
import de.h2b.scala.lib.util.config.{ Config, FileConfig }

/**
 * @author h2b
 */
package object example {

  private[example] def config (name: String, args: Array[String]): Config[String, String] = {
    val params = MainParameter("configfile", arity=1, required=true)
    val help = HelpParameter(Set("-h", "--help"))
    val cl = CommandLine(Set(params, help))
    try {
      cl.parse(args)
      if (help.value.getOrElse(false)) {
        println(cl.usage(name))
        sys.exit(0)
      }
    } catch {
      case t: Throwable ⇒
        Console.err.println(t.getMessage)
        Console.println()
        Console.println(cl.usage(name))
        sys.exit(1)
    }
    Try(FileConfig(getResource(getClass, params.values.get.head))) match {
      case Success(conf) ⇒
        conf
      case Failure(fail) ⇒
        println(fail.getMessage)
        sys.exit(1)
    }
  }

  private[example] def screen (h0: Int, background: Color, title: String): Screen = {
    val cell = GridLayout.onScreen(1, 1).fit(h0, h0)
    val screen = Screen(cell, title)
    screen.clear(background)
    screen
  }

  private[example] def world (p0: Point, l: Double)
      (sl: Int, title: String, backgroundOpt: Option[Color] = None): World = {
    val cell = GridLayout.onScreen(1, 1).fit(sl, sl)
    val world = World(Rectangle(p0, p0+Point(l,l)))(cell, title)
    for (bg ← backgroundOpt) world.clear(bg)
    world
  }

  private[example] def color (str: String): Color = str.toLowerCase() match {
    case "black" ⇒ Color.Black
  	case "blue" ⇒ Color.Blue
  	case "red" ⇒ Color.Red
  	case "green" ⇒ Color.Green
  	case "gray" ⇒ Color.Gray
  	case "cyan" ⇒ Color.Cyan
  	case "magenta" ⇒ Color.Magenta
  	case "yellow" ⇒ Color.Yellow
  	case "darkgray" ⇒ Color.DarkGray
  	case "orange" ⇒ Color.Orange
  	case "pink" ⇒ Color.Pink
  	case "lightgray" ⇒ Color.LightGray
  	case "white" ⇒ Color.White
  	case other ⇒ throw new IllegalArgumentException(s"color not found: $other")
  }

  private[example] def colorStream (n: Int, backgroundOpt: Option[Color] = None): Stream[Color] =
    Stream.continually(colorSeq(n, backgroundOpt).toStream).flatten

  private[example] def colorSeq (n: Int, backgroundOpt: Option[Color] = None): Seq[Color] = {
    val iter = backgroundOpt match {
      case Some(color) ⇒ ColorIterator(color)
      case None ⇒ ColorIterator()
    }
    iter.take(n).toSeq
  }

}
