/*
  SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package de.h2b.scala.simgraf.example

import scala.math.floor

import de.h2b.scala.lib.phys.Time
import de.h2b.scala.lib.simgraf.{ Color, Point, World }
import de.h2b.scala.lib.simgraf.shapes.Coloring
import de.h2b.scala.lib.util.Timer

/**
 * Draws repetitive patterns based on the equation of the circle.
 *
 * Fills the screen with colors that result from applying the equation of the
 * circle to the coordinates within
 * {@code [p0.x, p0.x+l] x [p0.y, p0.y+l]} (following J. E. Connett).
 *
 * Based on my Modula-2 module `Kreisquadrat` of 1986-88.
 *
 * Copyright (C) 1986-1988, 2015-2017 Hans-Hermann Bode
 *
 * @see [1] A. K. Dewdney, Spektr. d. Wiss. 1986/11, 6-13 (1986)
 * @version 3.1
 * @author h2b
 */
class CircleSquare private (val world: World, val colors: Seq[Color]) {

  private val m = colors.length

	run()

  private def run () = {
	  val t = Timer()
	  Coloring(connett).fill(world)
	  println("Time: " + Time(t.stop()))
  }

  private def connett (p: Point): Color = {
    val x = p.x
    val y = p.y
    val i = floor(x*x+y*y).toInt % m
    colors(i)
  }

}

object CircleSquare extends App {

	private val conf = config("circlesquare", args)

  try {
    val p0x = conf("p0.x").toDouble
    val p0y = conf("p0.y").toDouble
    val l = conf("l").toDouble
    val sl = conf("sl").toInt
    val nfg = conf("nfg").toInt
    val title = conf("title")
    apply(world(Point(p0x, p0y), l)(sl, title), colorSeq(nfg))
  } catch {
    case t: Throwable ⇒ println(t.getMessage)
  }

	/**
	 * Draws repetitive patterns based on the equation of the circle.
	 *
	 * @param world the coordinate system
	 * @param colors color sequence to be used
	 * @return
	 */
	def apply (world: World, colors: Seq[Color]) = {
	  new CircleSquare(world, colors)
	}

}
