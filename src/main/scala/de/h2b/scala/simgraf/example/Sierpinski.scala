/*
  SimGrafExamples -- Examples Using the SimGraf Scala Graphics Library

  Copyright 2016-2017 Hans-Hermann Bode

  Licensed under the EUPL V.1.1.
*/
package de.h2b.scala.simgraf.example

import de.h2b.scala.lib.simgraf.{ Color, Pixel, Screen }

/**
 * Draws Sierpiński curves.
 *
 * Based on my Modula-2 module Sierpinski from 1987/88.
 *
 * Copyright (C) 1987, 1988, 2015-2017 Hans-Hermann Bode
 *
 * @see N. Wirth, "Algorithmen und Datenstrukturen" (2. Aufl., Stuttgart: Teubner), 183-191 (1979).
 * @version 2.1
 * @author h2b
 */
class Sierpinski private (val n: Int, screen: Screen, colors: Stream[Color]) {

  private val h0 = screen.width min screen.height
	private var x, y, h = 0

  run()

	private def run () {
    val colIter = colors.iterator
    h = h0 / 4
    var x0 = 2 * h
    var y0 = 3 * h
    for (k <- 1 to n) { //draw Sierpinski curve of order k
      screen.activeColor = colIter.next()
    	x0 = x0 - h; h = h / 2; y0 = y0 + h
      x = x0; y = y0; screen.moveTo(Pixel(x, y))
      a(k); x = x + h; y = y - h; screen.drawTo(Pixel(x, y))
      b(k); x = x - h; y = y - h; screen.drawTo(Pixel(x, y))
      c(k); x = x - h; y = y + h; screen.drawTo(Pixel(x, y))
      d(k); x = x + h; y = y + h; screen.drawTo(Pixel(x, y))
    }
	}

  private def a (i: Int): Unit = {
    if (i>0) {
        a(i - 1); x = x + h; y = y - h; screen.drawTo(Pixel(x, y))
        b(i - 1); x = x + 2 * h; screen.drawTo(Pixel(x, y))
        d(i - 1); x = x + h; y = y + h; screen.drawTo(Pixel(x, y))
        a(i - 1)
    }
  }

  private def b (i: Int): Unit = {
    if (i>0) {
        b(i - 1); x = x - h; y = y - h; screen.drawTo(Pixel(x, y))
        c(i - 1); y = y - 2 * h; screen.drawTo(Pixel(x, y))
        a(i - 1); x = x + h; y = y - h; screen.drawTo(Pixel(x, y))
        b(i - 1)
    }
  }

  private def c (i: Int): Unit = {
    if (i>0) {
        c(i - 1); x = x - h; y = y + h; screen.drawTo(Pixel(x, y));
        d(i - 1); x = x - 2 * h; screen.drawTo(Pixel(x, y));
        b(i - 1); x = x - h; y = y - h; screen.drawTo(Pixel(x, y));
        c(i - 1)
    }
  }

  private def d (i: Int): Unit = {
    if (i>0) {
        d(i - 1); x = x + h; y = y + h; screen.drawTo(Pixel(x, y));
        a(i - 1); y = y + 2 * h; screen.drawTo(Pixel(x, y));
        c(i - 1); x = x - h; y = y + h; screen.drawTo(Pixel(x, y));
        d(i - 1)
    }
  }

}

object Sierpinski extends App {

	private val conf = config("sierpinski", args)

  try {
    val n = conf("n").toInt
    val h0 = conf("h0").toInt
    val bg = color(conf("bg"))
    val nfg = conf("nfg").toInt
    val title = conf("title")
    apply(n, screen(h0, bg, title), colorStream(nfg, Some(bg)))
  } catch {
    case t: Throwable ⇒ println(t.getMessage)
  }

	/**
   * Draws Sierpiński curves of order 1 to n.
   *
	 * @param n nesting depth
   * @param screen drawing area, should be quadratic with a width of h0=2*k
   * for some k>=n
   * @param colors stream of foreground colors to be used
   * @return
	 */
  def apply (n: Int, screen: Screen, colors: Stream[Color]): Sierpinski =
    new Sierpinski(n, screen, colors)

}
