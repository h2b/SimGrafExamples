name := "SimGraf-Examples"

scalaVersion := "2.12.2"

libraryDependencies ++= Seq(
	"de.h2b.scala.lib" %% "simgraf" % "1.1.0",
	"de.h2b.scala.lib" %% "utilib" % "0.4.1"
)
